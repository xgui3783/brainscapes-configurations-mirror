

See [diff](https://jugit.fz-juelich.de/t.dickscheid/brainscapes-configurations/-/compare/LATEST_TAG_PH...master?from_project_id=3484&straight=false)

See [viewer link](https://siibra-explorer-rc.apps.jsc.hbp.eu/)

Please document all deltas and validation:


| name | validator | validation | 
| --- | --- | --- |
| <change 1> | <person name> | [1] |

[1] notes on the validation